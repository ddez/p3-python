import unittest
from mysound import Sound


class TestSin(unittest.TestCase):

    def test_sin(self):
        sound = Sound(1)
        freq = 440
        amplitude = 10000
        sound.sin(freq, amplitude)

        # Test frequency
        expected_period = 44100 / freq
        self.assertAlmostEqual(sound.buffer[int(expected_period)], sound.buffer[100], delta=10)

        # Test amplitude
        expected_max = amplitude
        self.assertAlmostEqual(max(sound.buffer), expected_max, delta=10)
        expected_min = -amplitude
        self.assertAlmostEqual(min(sound.buffer), expected_min, delta=10)

