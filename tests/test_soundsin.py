import unittest
from mysoundsin import SoundSin

class TestSoundSin(unittest.TestCase):

    def test_init(self):
        sound = SoundSin(1, 500, 2000) #duracion frecuencia y amplitud
        self.assertEqual(len(sound.buffer), 44100)

    def test_frequency(self):
        sound = SoundSin(1, 500, 2000)#duracion frecuencia y amplitud
        expected_period = 44100 / 500
        self.assertAlmostEqual(sound.buffer[int(expected_period)], sound.buffer[88], delta=10)

    def test_amplitude(self):
        sound = SoundSin(1, 500, 2000)#duracion frecuencia y amplitud
        expected_max = 2000
        self.assertAlmostEqual(max(sound.buffer), expected_max, delta=10)
