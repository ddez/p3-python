import unittest
from mysound import Sound
from soundops import soundadd

class TestSoundAdd(unittest.TestCase):

    def test_equal_length(self):
        #prueba con sonidos de igual longitud
        s1 = Sound(1)
        s2 = Sound(1)
        result = soundadd(s1, s2)
        self.assertEqual(len(result.buffer), len(s1.buffer))

    def test_different_length(self):
        #prueba con sonidos de distinta longitud
        s1 = Sound(0.5)
        s2 = Sound(1)
        result = soundadd(s1, s2)
        self.assertEqual(len(result.buffer), len(s2.buffer))

    def test_values(self):
        #prueba con valores concretos que se suman bien
        s1 = Sound(0.5)
        s1.buffer = [1, 2, 3]
        s2 = Sound(1)
        s2.buffer = [4, 5, 6, 7]
        result = soundadd(s1, s2)
        expected = [5, 7, 9, 7]
        self.assertEqual(result.buffer, expected)