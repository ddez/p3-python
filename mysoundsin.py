from mysound import Sound


class SoundSin(Sound):
    # hereda de la clase sound

    def __init__(self, duration, frequency, amplitude):
        # este metodo acepta duracion frecuencia y amplitud
        super().__init__(duration)
        # usamos el super() para acceder a la clase padre

        self.frequency = frequency
        # guarda estos valores como atributos
        self.amplitude = amplitude

        super().sin(frequency, amplitude)
