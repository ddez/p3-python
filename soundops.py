
from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:

    # Cuenta el tamaño del buffer de cada sonido
    n_d1 = len(s1.buffer)
    n_d2 = len(s2.buffer)

    result = Sound(max(n_d1, n_d2) / s1.samples_second)

    # Add samples until the shorter sound ends
    nmin = min(n_d1, n_d2)

    for i in range(nmin):
        result.buffer[i] = s1.buffer[i] + s2.buffer[i]

    # Append remaining samples from longer sound
    if n_d1 > n_d2:
        result.buffer[nmin:] = s1.buffer[nmin:]
    else:
        result.buffer[nmin:] = s2.buffer[nmin:]
    return result
